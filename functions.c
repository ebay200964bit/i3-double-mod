#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>
#include <sys/stat.h>
#include <time.h>
#include <string.h>


void basicI3keyboardShortcuts(char keysPressed[]){
    if (strcmp(keysPressed, "SUPER + 1 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace \"1 \"");
    } else if (strcmp(keysPressed, "SUPER + 2 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace \"2 \"");
    } else if (strcmp(keysPressed, "SUPER + 3 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace \"3 \"");
    } else if (strcmp(keysPressed, "SUPER + 4 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace \"4 \"");
    } else if (strcmp(keysPressed, "SUPER + 5 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace \"5 \"");
    } else if (strcmp(keysPressed, "SUPER + 6 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace \"6 \"");
    } else if (strcmp(keysPressed, "SUPER + 7 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace \"7 \"");
    } else if (strcmp(keysPressed, "SUPER + 8 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace \"8 \"");
    } else if (strcmp(keysPressed, "SUPER + 9 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace \"9 \"");
    } else if (strcmp(keysPressed, "SUPER + 0 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace 10");
    } else if (strcmp(keysPressed, "SUPER + SUPER + 1 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace 11");
    } else if (strcmp(keysPressed, "SUPER + SUPER + 2 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace 12");
    } else if (strcmp(keysPressed, "SUPER + SUPER + 3 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace 13");
    } else if (strcmp(keysPressed, "SUPER + SUPER + 4 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace 14");
    } else if (strcmp(keysPressed, "SUPER + SUPER + 5 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace 15");
    } else if (strcmp(keysPressed, "SUPER + SUPER + 6 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace 16");
    } else if (strcmp(keysPressed, "SUPER + SUPER + 7 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace 17");
    } else if (strcmp(keysPressed, "SUPER + SUPER + 8 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace 18");
    } else if (strcmp(keysPressed, "SUPER + SUPER + 9 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace 19");
    } else if (strcmp(keysPressed, "SUPER + SUPER + 0 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) workspace 20");
    } else {
        //printf("not a core shortcut");
    }
    if (strcmp(keysPressed, "SUPER + SHIFT + 1 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace \"1 \"");
    } else if (strcmp(keysPressed, "SUPER + SHIFT + 2 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace \"2 \"");
    } else if (strcmp(keysPressed, "SUPER + SHIFT + 3 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace \"3 \"");
    } else if (strcmp(keysPressed, "SUPER + SHIFT + 4 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace \"4 \"");
    } else if (strcmp(keysPressed, "SUPER + SHIFT + 5 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace \"5 \"");
    } else if (strcmp(keysPressed, "SUPER + SHIFT + 6 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace \"6 \"");
    } else if (strcmp(keysPressed, "SUPER + SHIFT + 7 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace \"7 \"");
    } else if (strcmp(keysPressed, "SUPER + SHIFT + 8 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace \"8 \"");
    } else if (strcmp(keysPressed, "SUPER + SHIFT + 9 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace \"9 \"");
    } else if (strcmp(keysPressed, "SUPER + SHIFT + 0 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace 10");
    } else if (strcmp(keysPressed, "SUPER + SUPER + SHIFT + 1 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace 11");
    } else if (strcmp(keysPressed, "SUPER + SUPER + SHIFT + 2 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace 12");
    } else if (strcmp(keysPressed, "SUPER + SUPER + SHIFT + 3 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace 13");
    } else if (strcmp(keysPressed, "SUPER + SUPER + SHIFT + 4 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace 14");
    } else if (strcmp(keysPressed, "SUPER + SUPER + SHIFT + 5 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace 15");
    } else if (strcmp(keysPressed, "SUPER + SUPER + SHIFT + 6 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace 16");
    } else if (strcmp(keysPressed, "SUPER + SUPER + SHIFT + 7 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace 17");
    } else if (strcmp(keysPressed, "SUPER + SUPER + SHIFT + 8 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace 18");
    } else if (strcmp(keysPressed, "SUPER + SUPER + SHIFT + 9 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace 19");
    } else if (strcmp(keysPressed, "SUPER + SUPER + SHIFT + 0 + ") == 0) {
        system("i3-msg --socket=$(ls /run/user/1000/i3/ipc-socket.*) move container to workspace 20");
    } else {
        //printf("Not a c");
    }
}